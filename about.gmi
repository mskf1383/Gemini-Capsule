=> gemini://envs.net/~mskf1383/ بازگشت

# درباره
* یک زمینی،
* سرگردان در فضای بی‌کران،
* درون فضاپیمایش،
* در جست‌وجوی ناشناخته‌ها،
* تا بی‌نهایت…

## راه‌های ارتباطی
=> https://matrix.to/#/@mskf1383:tchncs.de ماتریکس
=> mailto:mskf1383@envs.net رایانامه

## در شبکه‌های اجتماعی
=> https://codeberg.org/mskf1383 کُدبِرگ
=> https://social.tchncs.de/@mskf1383 ماستودون
=> https://fediverse.blog/@/mskf1383 پُلوم
=> https://pixel.tchncs.de/mskf1383 پیکسل‌فِد
=> https://tube.tchncs.de/a/mskf1383/video-channels پیرتیوب
